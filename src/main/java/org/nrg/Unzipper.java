/**
 * Copyright (c) 2007,2011 Washington University
 */
package org.nrg;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

/**
 * Unpacks zip files
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class Unzipper extends Unpacker {
    private final Logger logger = LoggerFactory.getLogger(Unzipper.class);

    @Override
    public final void unpack(final File zipfile, final File dest) {
        final FileInputStream fis;
        try {
            IOException ioexception = null;
            fis = new FileInputStream(zipfile);
            try {
                publishStatus(zipfile, "unzipping");

                final BufferedInputStream bis = new BufferedInputStream(fis);
                try {
                    final ZipInputStream zis = new ZipInputStream(bis);
                    try {
                        unpack(zipfile, zis, dest == null ? zipfile.getParentFile() : dest);
                        publishSuccess(zipfile, "unzipped");
                    } catch (IOException e) {
                        throw ioexception = e;
                    } finally {
                        try {
                            zis.close();
                        } catch (IOException e)  {
                            throw ioexception = null == ioexception ? e : ioexception;
                        }
                    }
                } finally {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        throw ioexception = null == ioexception ? e : ioexception;
                    }
                }
            } finally {
                try {
                    fis.close();
                } catch (IOException e) {
                    throw null == ioexception ? e : ioexception;
                }
            }
        } catch (FileNotFoundException e) {
            publishFailure(zipfile, "unable to locate: " + e.getMessage());
            logger.error("could not find zipfile " + zipfile, e);
        } catch (IOException e) {
            publishFailure(zipfile, "unable to unpack " + zipfile + ": " + e.getMessage());
            logger.error("unable to unpack " + zipfile, e);
        }
    }

    private static final char notFileSeparator = '/' == File.separatorChar ? '\\' : '/';
    
    /**
     * Unpacks from a stream.
     * @param zis input stream
     * @param dest destination directory (must be non-null)
     * @throws IOException
     */
    public final void unpack(final Object control, final ZipInputStream zis, final File dest) throws IOException {
        dest.mkdirs();
        try {
            for (ZipEntry ze = zis.getNextEntry(); ze != null; ze = zis.getNextEntry()) {
                final String name = ze.getName().replace(notFileSeparator, File.separatorChar);
                final File outfile = new File(dest, name);
                logger.trace("extracting {} to {}", ze.getName(), outfile);
                if (ze.isDirectory()) {
                    outfile.mkdirs();
                    continue;
                } else if (outfile.exists()) {
                    publishWarning(outfile, "file exists, will not overwrite");
                    continue;
                }

                outfile.getParentFile().mkdirs();
                IOException ioexception = null;
                final FileOutputStream fos = new FileOutputStream(outfile);
                try {
                    ByteStreams.copy(zis, fos);
                } catch (IOException e) {
                    throw ioexception = e;
                } finally {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        throw null == ioexception ? e : ioexception;
                    }
                }
            }
        } catch (IOException e) {
            publishFailure(control, "unable to unpack: " + e.getMessage());
        }
    }
}