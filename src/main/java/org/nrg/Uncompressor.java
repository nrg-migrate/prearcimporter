/**
 * Copyright (c) 2007,2010 Washington University
 */
package org.nrg;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Unpack;

/**
 * Uncompresses a file using a specific Ant Unpack method.
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class Uncompressor extends Unpacker {
	private final Unpack unpacker;

	public Uncompressor(final Unpack unpacker, final Project project) {
		this.unpacker = unpacker;
		this.unpacker.setProject(project);
	}

	public void unpack(final File file, final File dest) {
		publishStatus(file, "extracting");
		unpacker.setSrc(file);
		if (dest != null) {
			dest.getParentFile().mkdirs();
			unpacker.setDest(dest);
		}

		try {
			unpacker.execute();
			file.delete();
		} catch (BuildException e) {
			publishFailure(file, e.getMessage());
		}
	}
}