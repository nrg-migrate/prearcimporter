/**
 * Copyright (c) 2007-2011 Washington University
 */
package org.nrg.dcm;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Move;
import org.dcm4che2.data.Tag;
import org.nrg.AbstractRestructurer;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.Utils;
import org.nrg.status.LoggerStatusReporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;

/**
 * Converts an arbitrary directory structure of DICOM files
 * into a structure we like.
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public final class Restructurer extends AbstractRestructurer {
    private final static String MULTIPLE_FILES = "(selected files)";
    private final static Set<DicomAttributeIndex> patientSelectionKeys = Collections.singleton(Attributes.PatientID);
    private final static String defaultSessionName = "session";
    private final static Set<DicomAttributeIndex> studyLabelKeys = Collections.singleton((DicomAttributeIndex)new FixedDicomAttributeIndex(Tag.StudyID));
    private final static Set<DicomAttributeIndex> seriesSelectionKeys = Collections.singleton(Attributes.SeriesInstanceUID);
    private final static Set<DicomAttributeIndex> allKeys;
    static {
        final Set<DicomAttributeIndex> keys = Sets.newLinkedHashSet();
        keys.add(Attributes.StudyInstanceUID);
        keys.addAll(patientSelectionKeys);
        keys.addAll(studyLabelKeys);
        keys.addAll(seriesSelectionKeys);
        keys.add(Attributes.Modality);
        keys.add(Attributes.SeriesNumber);
        allKeys = Collections.unmodifiableSet(keys);
    }
    private static final boolean decompressIsSupported = Decompress.isSupported();

    private final Logger logger = LoggerFactory.getLogger(Restructurer.class);

    private final Collection<File> infiles;
    private final File outdir;
    private final Project project;
    private final Set<File> studies = Sets.newLinkedHashSet();

    public Restructurer(final Collection<File> infiles, final File outdir) throws IOException {
        outdir.mkdirs();
        if (!outdir.isDirectory()) {
            throw new IOException(outdir + " is not a directory");
        }

        this.infiles = ImmutableSet.copyOf(infiles);
        this.outdir = outdir;
        this.project = new Project();
        project.setBaseDir(outdir);
    }

    public Restructurer(final File[] infiles, final File outdir) throws IOException {
        this(Arrays.asList(infiles), outdir);
    }

    public Restructurer(final File infile, final File outdir) throws IOException {
        this(Collections.singleton(infile), outdir);
    }

    public void run() {
        if (infiles.isEmpty()) {
            return;
        }

        final Object masterObj;
        if (1 == infiles.size()) {
            masterObj = infiles.iterator().next();
        } else {
            masterObj = MULTIPLE_FILES;
        }
        publishStatus(masterObj, "looking for DICOM files");

        outdir.mkdirs();

        // Find all DICOM files in the indir and sort them by session
        // (this is almost equivalent to DICOM study, but see comments for SessionDirectoryRecordFactory)
        final DicomMetadataStore fs;
        try {
            fs = EnumeratedMetadataStore.createHSQLDBBacked(allKeys);
            fs.add(infiles);
            publishStatus(masterObj, "found " + fs.getSize() + " DICOM files");
        } catch (IOException e) {
            publishFailure(masterObj, e.getMessage());
            return;
        } catch (SQLException e) {
            publishFailure(masterObj, e.getMessage());
            return;
        }

        try {
            final Set<String> studyUIDs;
            try {
                studyUIDs = fs.getUniqueValues(Attributes.StudyInstanceUID);
            } catch (ConversionFailureException e) {
                publishFailure(masterObj, e.getMessage());
                return;
            } catch (IOException e) {
                publishFailure(masterObj, e.getMessage());
                return;
            } catch (SQLException e) {
                publishFailure(masterObj, e.getMessage());
                return;
            }

            boolean warnedAboutDecompression = false;
            STUDIES: for (final String studyUID : studyUIDs) {
                final Map<DicomAttributeIndex,String> studySpec = Collections.singletonMap(Attributes.StudyInstanceUID, studyUID);

                final Set<DicomAttributeIndex> keys = Sets.newLinkedHashSet(patientSelectionKeys);
                keys.addAll(studyLabelKeys);
                keys.add(Attributes.Modality);

                final SetMultimap<DicomAttributeIndex,String> values;
                try {
                    final Map<DicomAttributeIndex,ConversionFailureException> failures = Maps.newHashMap();
                    values = fs.getUniqueValuesGiven(studySpec, keys, failures);
                    if (!failures.isEmpty()) {
                        publishFailure(masterObj, "Skipping study " + studyUID + ": " + failures.values());
                        continue STUDIES;
                    }
                } catch (IOException e) {
                    logger.error("skipping study " + studyUID, e);
                    publishFailure(masterObj, "Skipping study " + studyUID + ": " + e.getMessage());
                    continue STUDIES;
                } catch (SQLException e) {
                    logger.error("skipping study " + studyUID, e);
                    publishFailure(masterObj, "Skipping study " + studyUID + ": " + e.getMessage());
                    continue STUDIES;
                }

                final String label = buildLabel(values, patientSelectionKeys, defaultSessionName);
                publishStatus(masterObj, "found DICOM study " + label + " (" + studyUID + ")");

                final File studyDir = Utils.getUnique(outdir, label);
                studies.add(studyDir);
                final File scansDir = new File(studyDir, SCANS_DIR);
                scansDir.mkdirs();
                if (!studyDir.isDirectory()) {
                    publishFailure(studyDir, "unable to create image directory " + scansDir.getPath());
                    continue STUDIES;
                }

                final Set<String> seriesUIDs;
                try {
                    final Map<DicomAttributeIndex,ConversionFailureException> failures = Maps.newHashMap();
                    final SetMultimap<DicomAttributeIndex,String> seriesUIDm = fs.getUniqueValuesGiven(studySpec, seriesSelectionKeys, failures);
                    if (!failures.isEmpty()) {
                        publishFailure(studyDir, "unable to determine series selection keys for study " + label);
                        continue STUDIES;
                    }

                    seriesUIDs = seriesUIDm.get(Attributes.SeriesInstanceUID);
                    if (null == seriesUIDs) {
                        publishFailure(studyDir, "unable to determine Series Instance UIDs for study " + label);
                        continue STUDIES;
                    }
                } catch (IOException e) {
                    logger.error("Skipping study " + studyUID, e);
                    publishFailure(masterObj, "Skipping study " + studyUID + ": " + e.getMessage());
                    continue STUDIES;
                } catch (SQLException e) {
                    logger.error("Skipping study " + studyUID, e);
                    publishFailure(masterObj, "Skipping study " + studyUID + ": " + e.getMessage());
                    continue STUDIES;
                }

                boolean needsDecompression = false;
                for (final String seriesUID : seriesUIDs) {
                    final Map<DicomAttributeIndex,String> seriesSpec = Maps.newHashMap(studySpec);
                    seriesSpec.put(Attributes.SeriesInstanceUID, seriesUID);
                    final Set<DicomAttributeIndex> seriesAttrs = ImmutableSet.of(Attributes.SeriesNumber, Attributes.TransferSyntaxUID);

                    final Set<File> files;
                    final File scanDir;
                    try {
                        final Map<DicomAttributeIndex,ConversionFailureException> failures = Maps.newHashMap();
                        files = fs.getFilesForValues(seriesSpec, failures);
                        if (failures.isEmpty()) {
                            final SetMultimap<DicomAttributeIndex,String> m = fs.getUniqueValuesGiven(seriesSpec, seriesAttrs, failures);
                            final Set<String> seriesn = Sets.filter(m.get(Attributes.SeriesNumber), Predicates.notNull());
                            if (seriesn.isEmpty()) {
                                scanDir = scansDir;
                            } else {
                                final Iterator<String> sni = seriesn.iterator();
                                if (sni.hasNext()) {
                                    scanDir = new File(scansDir, sni.next());
                                } else {
                                    scanDir = scansDir;
                                }
                            }
                            for (final String tsuid : m.get(Attributes.TransferSyntaxUID)) {
                                if (Decompress.needsDecompress(tsuid)) {
                                    needsDecompression = true;
                                    break;
                                }
                            }
                        } else {
                            publishFailure(masterObj, "Skipping series " + seriesUID + " of session " + label + ": " + failures.values());
                            continue STUDIES;
                        }
                    } catch (IOException e) {
                        publishFailure(masterObj, "Skipping series " + seriesUID + " of session " + label + ": " + e.getMessage());
                        continue STUDIES;
                    } catch (SQLException e) {
                        publishFailure(masterObj, "Skipping series " + seriesUID + " of session " + label + ": " + e.getMessage());
                        continue STUDIES;
                    }

                    final File scanDicomDataDir = new File(scanDir, "DICOM");

                    for (File infile : files) {
                        if (needsDecompression) {
                            if (decompressIsSupported) {
                                try {
                                    infile = Decompress.dicomObject2File(Decompress.decompress_image(infile), infile);
                                }
                                catch (Throwable e) {
                                    publishFailure(masterObj, "Decompression error :" + e + ". Storing in original format.");
                                }
                            } else if (!warnedAboutDecompression) {
                                publishWarning(masterObj, "Files in " + label + " need decompression but support is not enabled."
                                        + " Storing in original format.");
                                warnedAboutDecompression = true;
                            }
                        }

                        // Construct a unique name for the file in the DICOM data directory for this scan.
                        final File outfile = Utils.getUnique(scanDicomDataDir, infile.getName());
                        final Move moveTask = new Move();
                        moveTask.setProject(project);
                        moveTask.setFile(infile);
                        moveTask.setTofile(outfile);
                        try {
                            moveTask.execute();
                        } catch (BuildException e) {
                            publishWarning(studyDir, e.getMessage());
                        }
                        if (!outfile.exists()) {
                            publishFailure(studyDir, "unable to move " + infile + " to " + outfile);
                            continue;
                        }
                    }
                    try {
                        fs.remove(Sets.filter(files, new Predicate<File>() {
                            public boolean apply(final File f) { return !f.exists(); }
                        }));
                    } catch (SQLException e) {
                        logger.warn("unable to remove some moved files from metadata store", e);
                    }
                }
            }

            // Remove any remaining empty directory trees.
            pruneDirectoryTree(infiles);

            publishSuccess(masterObj, "done finding DICOM");
        } finally {
            try {
                fs.close();
            } catch (IOException e) {
                logger.error("metadata database close failed", e);
            }
        }
    }


    private String buildLabel(final SetMultimap<DicomAttributeIndex,String> values,
            final Collection<DicomAttributeIndex> keys, final String defaultLabel) {
        for (final DicomAttributeIndex key : keys) {
            final Set<String> v = values.get(key);
            if (null != v) {
                final Iterator<String> i = v.iterator();
                if (i.hasNext()) {
                    final String s = i.next();
                    if (null != s) {
                        return s.replaceAll("[\\W]", "_");
                    }
                }
            }
        }
        return defaultLabel;
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.Restructurer#getSessions()
     */
    public Collection<File> getSessions() { return studies; }


    /**
     * @param args First argument is the pathname of the destination directory;
     * successive arguments are pathnames to be restructured.
     */
    public static void main(final String[] args) throws IOException {
        final File dest = new File(args[0]);
        for (int i = 1; i < args.length; i++) {
            final Restructurer r = new Restructurer(new File(args[i]), dest);
            r.addStatusListener(new LoggerStatusReporter(Restructurer.class));
            r.run();
            System.out.println("Sessions found in " + args[i] + ": " + r.studies);
        }
    }
}
