/**
 * Copyright (c) 2007-2011 Washington University
 */
package org.nrg;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

import org.nrg.status.BasicStatusPublisher;
import org.nrg.status.StatusListenerI;
import org.nrg.status.StatusMessage;
import org.nrg.status.StatusProducerI;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public abstract class AbstractRestructurer implements Restructurer {
    private final BasicStatusPublisher publisher;

    protected AbstractRestructurer(final BasicStatusPublisher publisher) {
        this.publisher = publisher;
    }

    protected AbstractRestructurer() {
        this(new BasicStatusPublisher());
    }

    protected final void publishStatus(final Object o, final CharSequence message) {
        publisher.publish(new StatusMessage(o, StatusMessage.Status.PROCESSING, message));
    }

    protected final void publishWarning(final Object o, final CharSequence message) {
        publisher.publish(new StatusMessage(o, StatusMessage.Status.WARNING, message));
    }

    protected final void publishFailure(final Object o, final CharSequence message) {
        publisher.publish(new StatusMessage(o, StatusMessage.Status.FAILED, message));
    }

    protected final void publishSuccess(final Object o, final CharSequence message) {
        publisher.publish(new StatusMessage(o, StatusMessage.Status.COMPLETED, message));
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.status.StatusProducerI#addStatusListener(org.nrg.status.StatusListenerI)
     */
    public final void addStatusListener(final StatusListenerI l) {
        publisher.addStatusListener(l);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.status.StatusProducerI#removeStatusListener(org.nrg.status.StatusListenerI)
     */
    public final void removeStatusListener(final StatusListenerI l) {
        publisher.removeStatusListener(l);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    public final Iterator<File> iterator() { 
        return getSessions().iterator();
    }

    /**
     * Walk through the directory trees starting at roots.  Remove all empty
     * directories, so that if the tree contains no regular files, all the
     * directories (including root) will be removed.
     * @param roots Root directories of tree to be pruned
     */
    public static final Collection<File> pruneDirectoryTree(final Collection<File> roots) {
        final Collection<File> processed = new HashSet<File>();
        final Collection<File> remaining = new LinkedList<File>();

        final Stack<File> stack = new Stack<File>();
        for (final File root : roots) {
            stack.push(root);
        }

        while (!stack.empty()) {
            final File dir = stack.peek();
            if (dir.isDirectory()) {
                final File[] files = dir.listFiles();
                if (files.length == 0) {
                    // Directory is empty: delete it and move on.
                    dir.delete();
                } else for (final File f : files) {
                    // Directory has some contents; if any of them are subdirectories,
                    // push them onto the stack.
                    if (f.isDirectory() && !processed.contains(dir)) {
                        stack.push(f);
                    } else {
                        remaining.add(f);
                    }
                }

                // If no subdirs were added to the stack, we're done with this directory.
                if (dir.equals(stack.peek())) {
                    stack.pop();
                }
            } else {
                remaining.add(dir);
                stack.pop();
            }

            processed.add(dir);
        }
        return remaining;
    }

    /**
     * Walk through the directory trees starting at roots.  Remove all empty
     * directories, so that if the tree contains no regular files, all the
     * directories (including root) will be removed.
     * @param roots Root directories of tree to be pruned
     */
    public static final Collection<File> pruneDirectoryTree(final File...roots) {
        return pruneDirectoryTree(Arrays.asList(roots));
    }
}
