/**
 * Copyright (c) 2007-2011 Washington University
 */
package org.nrg;

import java.io.File;
import java.util.Collection;

import org.nrg.status.StatusProducerI;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public interface Restructurer extends StatusProducerI,Runnable,Iterable<File> {
    public static final String SCANS_DIR = "SCANS";

    Collection<File> getSessions();
}
