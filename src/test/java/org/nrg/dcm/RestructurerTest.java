/**
 * Copyright (c) 2007,2008,2010,2011 Washington University
 */
package org.nrg.dcm;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Copy;
import org.apache.tools.ant.taskdefs.Delete;
import org.apache.tools.ant.types.FileSet;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class RestructurerTest {
	private final static File testDataDir = new File(System.getProperty("sample.data.dir"));
	private final static File tmpDir = new File(System.getProperty("java.io.tmpdir"));

	private final Project antProject = new Project();

	public RestructurerTest() {
		antProject.setBaseDir(testDataDir);
	}

	/**
	 * Test method for {@link org.nrg.dcm.Restructurer#Restructurer(java.io.File, java.io.File)}.
	 */
	@Test
	public void testRestructurerFileFile() throws IOException {
		// Make a working copy of the data.
		final Copy copy = new Copy();
		copy.setProject(antProject);
		final FileSet fs = new FileSet();
		fs.setDir(testDataDir);
		fs.setExcludes("*.xml,*.log");
		copy.addFileset(fs);
		final File copyDir = File.createTempFile("test-data", "copy");
		copyDir.delete();
		copy.setTodir(copyDir);
		copy.execute();

		final File o1 = new File(tmpDir, "r1");
		final Delete delete = new Delete();
		delete.setProject(antProject);
		delete.setDir(o1);
		delete.execute();

		assertFalse(o1.exists());
		final Restructurer r1 = new Restructurer(copyDir, o1);
		r1.run();
		final String[] remaining = copyDir.list();
		assertNull(remaining);	// if this fails, verify that the original directory contained only DICOM.
		copyDir.delete();
		assertFalse(copyDir.exists());	// should have moved all files

		// check for exactly one session directory
		assertEquals(1, o1.list().length);
		final File sessionDir = new File(o1, "Sample_ID");
		assertTrue(sessionDir.isDirectory());

		// containing three scans
		final File rawDir = new File(sessionDir, Restructurer.SCANS_DIR);
		assertTrue(rawDir.isDirectory());
		assertEquals(3, rawDir.list().length);

		// total of 528 .dcm.gz files
		int fileCount = 0;
		for (final File scanDir : rawDir.listFiles()) {
			final File dicomDir = new File(scanDir, "DICOM");
			fileCount += dicomDir.list(new FilenameFilter() {
				public boolean accept(final File dir, final String name) {
					return name.endsWith(".dcm.gz");
				}
			}).length;
		}
		assertEquals(528, fileCount);

		assertNotNull(r1);
		assertEquals(1, r1.getSessions().size());
		for (final File studyDir : r1) {
			assertEquals(o1, studyDir.getParentFile());
		}

		delete.setDir(o1);
		delete.execute();
	}

	/**
	 * Test method for {@link org.nrg.dcm.Restructurer#iterator()}.
	 */
	@Test
	public final void testIterator() {
		// fail("Not yet implemented"); // TODO
	}

}
